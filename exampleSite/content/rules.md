---
title: "Rules"
date: 2020-04-27 00:23:13 +0000 UTC
description: "Rules of Conduct"
keywords:
draft: false
showLastmod: true
meta: true
vh50: false
type: "page"
---

### Rules:

1. Be excellent & kind to each other.  
2. Ops have final say.  
3. No illegal things (inherited from Freenode's ToS).  

{{% details title="the same rules in a disclosure widget..." %}}

### Rules:

1. Be excellent & kind to each other.  
2. Ops have final say.  
3. No illegal things (inherited from Freenode's ToS).  

{{% /details %}}

{{% details title="the same rules in yet another disclosure widget..." %}}

### Rules:

1. Be excellent & kind to each other.  
2. Ops have final say.  
3. No illegal things (inherited from Freenode's ToS).  

{{% /details %}}
