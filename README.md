# hehe

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

a simple hugo theme for https://friends.computer

# features

- lack of anti-features (no remote sources/fonts/etc..., no javascript, ...)

# config

take a look at the example site... elaborate description following within the next couple of days, maybe...

# how to

create a new hugo site skeleton  
`hugo new site friends.computer`

add the theme to your new site  
`cd friends.computer`
`git clone https://gitlab.com/friends.computer/hehe themes/hehe`

copy over the example config file  
`cp themes/hehe/exampleSite/config.yaml .`

delete the default config file  
`rm config.toml`

take a look at the example files, then create default pages accordingly...  
`hugo new about`
`hugo new news/1.md`
`...`

create and minify your site  
`hugo --minify`

deploy with `rsync` or whatever...  
`rsync -aP --group --owner --delete public/ [USER@]HOST:DEST`

# kudos to...

- [**Fira Code**](https://github.com/tonsky/FiraCode)
- [**Font Awesome Free**](https://github.com/FortAwesome/Font-Awesome)
- [**Font-Logos**](https://github.com/lukas-w/font-logos)
